﻿CREATE SYNONYM [sys_dwh].[sp_addextendedproperty] FOR [dhw_self].[sp_addextendedproperty];






GO
EXECUTE sp_addextendedproperty @name = N'is_ssas', @value = N'0', @level0type = N'SCHEMA', @level0name = N'sys_dwh', @level1type = N'SYNONYM', @level1name = N'sp_addextendedproperty';


GO
EXECUTE sp_addextendedproperty @name = N'is_repo_managed', @value = N'0', @level0type = N'SCHEMA', @level0name = N'sys_dwh', @level1type = N'SYNONYM', @level1name = N'sp_addextendedproperty';

